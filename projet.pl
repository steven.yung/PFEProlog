sommet_adjacent(X,R) :-
	sommet([_,[X|R]]),
	!.

degree_sommet(X) :-
	sommet([[_|_],[X|T]]),
	Acc is 0,
	compter_degree(T,Acc),
	!.

compter_degree([],Acc) :-
	write(Acc),
	!.

compter_degree([_|T],Acc) :-
	NewAcc is Acc + 1,
	compter_degree(T,NewAcc),
	!.

liste_sommet(R) :-
	sommet([_, [R|_]]).

appartient(X,Sommet) :- 
	sommet([Etiquette,[Sommet|_]]),
	member(X,Etiquette).

sommet_avec([X],Sommet) :- 
	appartient(X,Sommet).

sommet_avec([H|T],Sommet) :- 
	appartient(H,Sommet), 
	sommet_avec(T,Sommet).

liste_sommet_avec([H|T],Resultat) :-
	bagof(Sommet,sommet_avec([H|T],Sommet),Resultat).

parcours_liste([X],Y,Sommet) :-
	member(X,Y),
	write(Sommet),
	write(' -- '),
	writeln(X),
	!.

parcours_liste([X],Y,Sommet) :-
	!.

parcours_liste([H|T],Y,Sommet) :-
	member(H,Y),
	write(Sommet),
	write(' -- '),
	write(H),
	writeln(';'),
	parcours_liste(T,Y,Sommet).

parcours_liste([_|T],Y,Sommet) :-
	parcours_liste(T,Y,Sommet).

ecrire_sous_graphe([_]) :-
	!.

ecrire_sous_graphe([H|T]) :-
	sommet_adjacent(H,R),
	parcours_liste(T,R,H),
	ecrire_sous_graphe(T).
	

sous_graphe(Etiquette) :-
	changement_sortie,
	debut_fichier_dot('sous_graphe'),
	liste_sommet_avec(Etiquette,R),
	ecrire_sous_graphe(R),
	fin_fichier_dot,
	fin_changement_sortie,
	!.
	
charger_donnees :-
	consult('donnees.pl').

changement_sortie :-
	tell('graphe.dot').

debut_fichier_dot(X) :-
	write('graph '),
	write(X),
	writeln(' {').
	
fin_fichier_dot :-
	writeln('}').

fin_changement_sortie :-
	told.